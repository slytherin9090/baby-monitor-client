const { app, BrowserWindow } = require('electron')

let mainWindow

function createWindow() {
  // Create the browser window
  let displays = require('electron').screen.getAllDisplays()
  let externalDisplay = displays[0];

  if (externalDisplay) {
    mainWindow = new BrowserWindow({
      width: 300,
      height: 220,
      webPreferences: {
        nodeIntegration: true
      },
      alwaysOnTop: true,
      titleBarStyle: 'hidden',
      x: externalDisplay.bounds.width,
      y: externalDisplay.bounds.height
    });

    mainWindow.loadURL('http://jaspers-imac.local:21000');
    mainWindow.setVisibleOnAllWorkspaces(true);
    mainWindow.setBackgroundColor('#111');
    mainWindow.on('closed', function () {
      mainWindow = null
    })

    mainWindow.webContents.on('did-finish-load', function () {
      mainWindow.webContents.insertCSS(`
        .feed{ -webkit-app-region: drag; } img { user-select:none; pointer-events:none}'
      `)
    });
  }
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  app.quit();
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})